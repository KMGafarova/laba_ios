//
//  AuthViewController.swift
//  LessonTwoHW
//
//  Created by user on 03.07.2022.
//

import UIKit

class AuthViewController: UIViewController {
    
    @IBOutlet weak var userNumberTextField: UITextField!
    @IBOutlet weak var confirmButton: UIButton!
    
    
    @IBAction func confirmDidTap(_ sender: Any) {
        guard let number = userNumberTextField.text else { return }
        let numberSubstring = number.prefix(5)
        
        if numberSubstring == "89087" {
            guard let contactTVC = storyboard?.instantiateViewController(withIdentifier: "ContactTableViewController") else { return }
            navigationController?.pushViewController(contactTVC, animated: true)
            
        } else {
            guard let incorrectNumberVC = storyboard?.instantiateViewController(withIdentifier: "IncorrectNumberViewController") else { return }
            present(incorrectNumberVC, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        userNumberTextField.text = nil
    }
}
