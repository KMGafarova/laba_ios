//
//  SingleContactViewController.swift
//  LessonTwoHW
//
//  Created by user on 03.07.2022.
//

import UIKit

class SingleContactViewController: UIViewController {
    
    var contact: Contact?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var telegramLabel: UILabel!
    
    
    @IBAction func closeDidTap(_ sender: Any) {
        dismiss(animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let contact = contact {
            nameLabel.text = contact.name
            numberLabel.text = String(contact.number)
            companyLabel.text = contact.company
            addressLabel.text = contact.address
            emailLabel.text = contact.email
            telegramLabel.text = contact.telegram
        }
    }
}
