//
//  ContactTableViewController.swift
//  LessonTwoHW
//
//  Created by user on 03.07.2022.
//

struct Contact {
    let name: String
    let number: Int
    let company: String
    let address: String
    let email: String
    let telegram: String
}

import UIKit

class ContactTableViewController: UITableViewController {
    
    let contact: [Contact] = [
        Contact(name: "Aliya", number: 89355462819, company: "School", address: "Lusnikova, 9", email: "aliya12@mail.ru", telegram: "@alya_alya"),
        Contact(name: "Ekaterina", number: 89176548880, company: "Universaty", address: "Bolshaya-Krasnaya, 55", email: "katyaaa@mail.ru", telegram: "@kamzavr"),
        Contact(name: "Anton", number: 89393934840, company: "Universaty", address: "Prospect Pobedy, 33", email: "antosha@mail.ru", telegram: "@pupupu"),
        Contact(name: "Ramil", number: 89273091122, company: "Universaty", address: "Absalyamova, 15", email: "neRamil@mail.ru", telegram: "@prosto_genyi")
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contact.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: "ContactTableViewCell",
            for: indexPath
        ) as? ContactTableViewCell else { return UITableViewCell() }
        
        cell.nameLabel.text = contact[indexPath.row].name
        cell.numberLabel.text = String(contact[indexPath.row].number)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        120
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let contactVC = storyboard?.instantiateViewController(withIdentifier: "SingleContactViewController") as? SingleContactViewController else { return }
        contactVC.contact = contact[indexPath.row]
        present(contactVC, animated: true)
    }
}
