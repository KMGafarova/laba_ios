import UIKit

protocol HomeworkService {
    // Функция деления с остатком, должна вернуть в первой части результат деления, во второй части остаток.
    func divideWithRemainder(_ x: Int, by y: Int) -> (Int, Int)
    
    // Функция должна вернуть числа фибоначчи.
    func fibonacci(n: Int) -> [Int]
    
    // Функция должна выполнить сортировку пузырьком.
    func sort(rawArray: [Int]) -> [Int]
    
    // Функция должна преобразовать массив строк в массив первых символов строки.
    func firstLetter(strings: [String]) -> [Character]
    
    // Функция должна отфильтровать массив по условию, которое приходит в параметре `condition`. (Нельзя юзать `filter` у `Array`)
    func filter(array: [Int], condition: ((Int) -> Bool)) -> [Int]
}

struct Homework: HomeworkService
{
    func divideWithRemainder(_ x: Int, by y: Int) -> (Int, Int)
    {
        return((x/y, x%y))
    }
    
    func fibonacci(n: Int) -> [Int]
    {
        var fibArray = [Int]()
        if n == 0 { return [0] }
        var a = 0
        fibArray.append(a)
        var b = 1
        fibArray.append(b)
        
        for _ in 0..<n    {
            let c = a + b;
            fibArray.append(c)
            a = b;
            b = c;
        }
        return fibArray;
    }
    
    func sort(rawArray: [Int]) -> [Int]
    {
        var array = rawArray
        for i in 1..<array.count {
            for j in 0..<array.count-i  {
                if rawArray[j] > array[j + 1] {
                    array.swapAt(j, j+1)
                }
            }
        }
        return array
    }
    
    func firstLetter(strings: [String]) -> [Character]
    {
        var charArray = [Character]()
        for i in 0..<strings.count {
            charArray.append(Array(strings[i])[0])
        }
        return charArray
    }
    
    func filter(array: [Int], condition: ((Int) -> Bool)) -> [Int] {
        var filteredArray = [Int]()
        for i in 0..<array.count
        {
            let number = array[i]
            if condition(number) { filteredArray.append(number) }
        }
        return filteredArray
    }
}

let work = Homework()
work.divideWithRemainder(5, by: 3)
work.fibonacci(n:5)
let array = [4,-2,7,8,-5]
work.sort(rawArray:array)
let text = ["human", "elephant", "letter", "pig"]
work.firstLetter(strings: text)

func isPositive(number: Int) -> Bool  {
    number>0
}

work.filter(array:array, condition:isPositive)
