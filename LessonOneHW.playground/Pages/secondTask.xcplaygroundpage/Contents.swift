import UIKit

enum TargetAudience {
    case child
    case preTeen
    case teenager
    case youndAdult
    case middleAged
    case elderly
}

enum Genre {
    case fiction
    case nonFiction
    case chickLit
    case scienceFiction
    case fantasy
    case travel
    case autobiography
}

enum Weekday {
    case monday
    case tuesday
    case thuesday
    case wednesday
    case thirsday
    case friday
    case saturday
    case sunday
}

enum Status {
    case notFinished
    case finished
}


protocol Event {
    var name: String {get}
    var audience: [TargetAudience] {get}
    var weekday: Weekday {get}
    var speaker: String {get set}
    var time: DateComponents {get}
    var status: Status {get set}
    
    func makeAnnouncement()
    func holdAnEvent()
}

class PhysicLection: Event {
    
    var name: String
    var audience: [TargetAudience] = [TargetAudience.teenager,                                  TargetAudience.youndAdult,
                                      TargetAudience.middleAged,
                                      TargetAudience.elderly]
    var weekday: Weekday = Weekday.thuesday
    var time: DateComponents = DateComponents(hour:16, minute:0)
    var speaker: String
    var status: Status = Status.notFinished
    
    init(name:String, speaker: String) {
        self.name = name
        self.speaker = speaker
    }
    
    func makeAnnouncement() {
        print("Lection \(name) will be held on \(weekday) at \(time). Speaker is \(speaker). We are waiting for you!")
    }
    
    func holdAnEvent() {
        status = Status.finished
    }
    
    func changeSpeaker(name:String) {
        speaker = name
    }
}

class BookClub: Event {
    var name: String
    var audience: [TargetAudience] = [TargetAudience.child]
    var weekday: Weekday = Weekday.saturday
    var time: DateComponents = DateComponents(hour:13, minute:30)
    var speaker: String = "Anna Ivanovna"
    var status: Status = Status.notFinished
    
    init(name:String) {
        self.name = name
    }
    
    func makeAnnouncement() {
        print("New book club meeting will be about \(name) and will held on \(weekday) at \(time). Speaker as usual is \(speaker). We are waiting for you!")
    }
    
    func holdAnEvent() {
        status = Status.finished
    }
    
    func changeSpeaker(name:String) {
        speaker = name
    }
}

class Human {
    var name: String
    var surname: String
    var middleName: String
    var birthday: Date
    var passport: Int
    var age: Int
    
    init(name: String, surname: String, middleName: String, birthday: Date, passport: Int) {
        self.name = name
        self.surname = surname
        self.middleName = middleName
        self.birthday = birthday
        self.passport = passport
        self.age = Human.CalculateAge(birthday)
    }
    
    private static func CalculateAge(_ birth: Date)->Int {
        let now = Date()
        let calendar = Calendar.current
        let ageComponents = calendar.dateComponents([.year], from: birth, to: now)
        return ageComponents.year!
    }
    
    func say() {
        print("I am human")
    }
}

class Reader: Human {
    
    let libraryCard: Card
    
    init(name: String, surname: String, middleName: String, birthday: Date, passport: Int, libraryCard: Card) {
        self.libraryCard = libraryCard
        super.init(name:name, surname:surname, middleName:middleName, birthday:birthday, passport:passport)
    }
    
    override func say() {
        print("I am reader")
    }
}

class Librarian: Human {
    
    override func say() {
        print("I am librarian")
    }
    
    func registerReader(name:  String, surname: String, middleName: String, birthday: Date, passport: Int, libraryCard: Card) -> Reader {
        return Reader(name:name, surname:surname, middleName:middleName, birthday:birthday, passport:passport, libraryCard:libraryCard)
    }
}

struct Card {
    let number:Int
    let registrationDate: Date
    
    init(number: Int, registrationDate: Date) {
        self.number = number
        self.registrationDate = registrationDate
    }
}

protocol PaperExemplar {
    func isInStock() -> Bool
    mutating func checkOut()
}

struct Book: PaperExemplar {
    
    private var availability: Bool
    let name: String
    let genre: Genre
    let author: String
    let year: Int
    var count: Int = 0
    
    init(availability: Bool, name: String, genre: Genre, author: String, year: Int, count: Int) {
        self.availability = availability
        self.name = name
        self.genre = genre
        self.author = author
        self.year = year
        self.count = count
    }
    
    func isInStock() -> Bool {
        return availability
    }
    
    mutating func checkOut() {
        count -= 1
    }
}

struct NewsPaper: PaperExemplar {
    
    private var availability:  Bool
    let name: String
    let redactors: [String]
    let number: Int
    let date: Date
    var count: Int = 0
    
    init(availability: Bool, name:String, redactors: [String], number: Int, date: Date, count: Int)
    {
        self.availability = availability
        self.name = name
        self.redactors = redactors
        self.number = number
        self.date = date
        self.count = count
    }
    
    func isInStock() -> Bool {
        return availability
    }
    
    mutating func checkOut() {
        count -= 1
    }
}
